package re.istic.magiteammm.magicrecipe;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import re.istic.magiteammm.magicrecipe.domain.Recipe;


public class MyMapFragment extends Fragment {

    /**    *  Attributes  *    **/
    private GoogleMap myGoogleMap; // Might be null if Google Play services APK is not available.
    private List<Recipe> listRecipes;


    /**    *  LifeCycle Methods  *    **/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        setUpMapIfNeeded();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     *
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (myGoogleMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            myGoogleMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMap();
            // Check if we were successful in obtaining the map.
            if (myGoogleMap != null) {
                generateRandomRecipies();
                loadRecipiesPositions();
            }
        }
    }



    /**
     * Add Default Test Recipies in the collection
     */
    private void generateRandomRecipies() {
        listRecipes = new ArrayList<>();
        Recipe r;
        for(int i=0; i<3; i++) {
            r = new Recipe();
            r.setName("Recette Test " + i);
            r.setLatitude(10 + i * 4);
            r.setLatitude(20 + i * 5);
            listRecipes.add(r);
        }
    }

    /**
     * TODO: POSITIONS OF RECIPIES - LOAD RECIPES POSITION FROM FIREBASE /!\
     */
    private void loadRecipiesPositions() {
        for(Recipe recipe : listRecipes){
            myGoogleMap.addMarker(new MarkerOptions().position(new LatLng(recipe.getLongitude(),
                    recipe.getLatitude())).title(recipe.getName()));
        }

    }

}
