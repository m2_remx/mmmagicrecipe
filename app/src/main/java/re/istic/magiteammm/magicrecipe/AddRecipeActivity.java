package re.istic.magiteammm.magicrecipe;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import re.istic.magiteammm.magicrecipe.domain.Recipe;

/**
 * Created by MagicTeam on 21/03/16.
 */
public class AddRecipeActivity extends AppCompatActivity implements View.OnClickListener {

    /**     -       Attributes        -      **/
    private Recipe recipe;

    EditText title;
    EditText description;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_recipe);


        recipe = new Recipe();

        //** Init GUI Components **/
        initSpinnerSizeOfRecipe();
        initButtonValidNewRecipe();
        initSpinnerDurationOfRecipe();

        title = (EditText) findViewById(R.id.text_title);
        title.requestFocus();

    }


    /**
     *
     */
    private void initSpinnerSizeOfRecipe(){

        final int SIZEMAX = 10;
        String[] state= new String[SIZEMAX];
        for(Integer i=1; i < SIZEMAX; i++) {
            state[i-1] = i.toString();
        }


        ArrayAdapter<String> adapter_state = new ArrayAdapter<String>(this,  android.R.layout.simple_spinner_item, state);
        adapter_state.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner spinnerSizeOfRecipe = (Spinner) findViewById(R.id.spinner_sizeinpeople);
        spinnerSizeOfRecipe.setAdapter(adapter_state);


        /*
        spinnerSizeOfRecipe.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println(parent.getItemAtPosition(position).toString());
                recipe.setSizeInPeople(Integer.parseInt(parent.getItemAtPosition(position).toString()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                recipe.setSizeInPeople(0);
            }
        });*/

    }

    /**
     *
     */
    private void initSpinnerDurationOfRecipe(){
        final int HOURMAX = 23;
        final int MINUTEMAX = 59;
        String[] hours= new String[HOURMAX];
        String[] minutes= new String[MINUTEMAX];
        for(Integer i=1; i < HOURMAX; i++) {
            if(i<10) {
                hours[i-1]="0" + i.toString();
            }
            else {
                hours[i - 1] = i.toString();
            }
        }
        for(Integer i=1; i < MINUTEMAX; i++) {
            if(i<10) {
                minutes[i-1]="0" + i.toString();
            }
            else {
                minutes[i - 1] = i.toString();
            }
        }
        ArrayAdapter<String> adapter_h = new ArrayAdapter<String>(this,  android.R.layout.simple_spinner_item, hours);
        Spinner spinnerDurationHour = (Spinner) findViewById(R.id.spinner_durationhour);
        spinnerDurationHour.setAdapter(adapter_h);

        /*
        spinnerDurationHour.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                recipe.setDurationMinutes(Integer.parseInt(parent.getItemAtPosition(position).toString()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });*/

        ArrayAdapter<String> adapter_m = new ArrayAdapter<String>(this,  android.R.layout.simple_spinner_item, hours);
        Spinner spinnerDurationMinute = (Spinner) findViewById(R.id.spinner_durationminute);
        spinnerDurationMinute.setAdapter(adapter_m);

        /*
        spinnerDurationMinute.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                recipe.setDurationMinutes(Integer.parseInt(parent.getItemAtPosition(position).toString()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });*/



    }

    /**
     *
     */
    private void initButtonValidNewRecipe(){
        Button buttonValidNewRecipe = (Button) findViewById(R.id.button_validnewrecipe);
        buttonValidNewRecipe.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Toast.makeText(this, "Ok !", Toast.LENGTH_SHORT);

        /** * * * * * * * * * * * * * * * * * * *
         * TODO: INSERT recipe into FIREBASE    *
         * * * * * * * * * * * * * * * * * * * */

        finish();
    }
}
