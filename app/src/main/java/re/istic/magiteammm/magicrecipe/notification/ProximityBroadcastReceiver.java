package re.istic.magiteammm.magicrecipe.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.widget.Toast;

/**
 * Created by callimom on 23/03/16.
 */
public class ProximityBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String message;

        String key = LocationManager.KEY_PROXIMITY_ENTERING;
        Boolean entering = intent.getBooleanExtra(key, false);

        if (entering) {
            Toast.makeText(context, "* You're near a Magic Recipe *", Toast.LENGTH_LONG).show();

        }
    }
}
