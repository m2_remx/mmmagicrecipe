package re.istic.magiteammm.magicrecipe.notification;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;

/**
 * Created by MagicTeam on 21/03/16.
 */
public class GeolocalizationUTIL {

    /**
     *
     * @return
     */
    static public Double[] getLocalization(Activity activity) {
        String locationContext = Context.LOCATION_SERVICE;
        LocationManager locationManager = (LocationManager) activity.getSystemService(locationContext);
        String provider = LocationManager.GPS_PROVIDER;
        // String provider = LocationManager.NETWORK_PROVIDER;
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null;
        }
        Location location = locationManager.getLastKnownLocation(provider);

        double latitude = 42;
        double longitude = 42;
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();

        }
        Double[] coordinate = new Double[]{latitude,longitude};
        return coordinate;
    }

}
