package re.istic.magiteammm.magicrecipe.domain;

/**
 * Created by MagicTeam on 21/03/16.
 */
public class Recipe {

    /**     -       Attributes        -      **/

    /** Name or Title of the recipe **/
    private String name;
    /** Size of the meal. Number of people **/
    private int sizeInPeople;
    /** Content of the recipe **/
    private String description;
    /** part of total duration: minutes needed to execute the recipe **/
    private int durationMinutes;
    /** part of total duration: hours needed to execute the recipe **/
    private int durationHour;
    /** latitude of the recipe **/
    private double latitude;
    /** longitude of the recipe **/
    private double longitude;



    /**     -       Constructors        -      **/

    /**
     *  Default Constructor
     */
    public Recipe(){}

    /**
     * Constructor with parameters
     * @param name  :   Name or Title of the recipe
     * @param sizeInPeople  :   Size of the meal. Number of people
     * @param description   :   Content of the recipe
     * @param durationMinutes :   Time needed to execute the recipe
     */
    public Recipe(String name, int sizeInPeople, String description, int durationMinutes) {
        this.name = name;
        this.sizeInPeople = sizeInPeople;
        this.description = description;
        this.durationMinutes = durationMinutes;
    }


    /**     -       Getter & Setters        -      **/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSizeInPeople() {
        return sizeInPeople;
    }

    public void setSizeInPeople(int sizeInPeople) {
        this.sizeInPeople = sizeInPeople;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getDurationMinutes() {
        return durationMinutes;
    }

    public void setDurationMinutes(int durationMinutes) {
        this.durationMinutes = durationMinutes;
    }


    public int getDurationHour() {
        return durationHour;
    }

    public void setDurationHour(int durationHour) {
        this.durationHour = durationHour;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
