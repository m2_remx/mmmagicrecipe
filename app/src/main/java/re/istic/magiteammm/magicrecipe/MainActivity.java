package re.istic.magiteammm.magicrecipe;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    /**    *  Attributes  *    **/
    private boolean isLarge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /**  Detect if we're on a smart phone or a TABLET  **/
        isLarge = ((getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK) ==
                Configuration.SCREENLAYOUT_SIZE_LARGE) ||
                ((getResources().getConfiguration().screenLayout &
                        Configuration.SCREENLAYOUT_SIZE_MASK) ==
                        Configuration.SCREENLAYOUT_SIZE_XLARGE);

        /**  Add the First Fragment of the App : The Map  **/
        Fragment mapFragment = new MyMapFragment();
        FragmentManager fragmentManager =  getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container_fragment_map, mapFragment).commit();


        initButtons();

    }




    private void initButtons(){
        Button buttonAddNewRecipe = (Button)findViewById(R.id.button_addnewrecipe);
        buttonAddNewRecipe.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intentAddNewRecipe = new Intent(this, AddRecipeActivity.class);
        startActivity(intentAddNewRecipe);

    }

}
